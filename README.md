# Incredible web test [demo](https://iwt.nanforce.com/)

### This project was bootstrapped with

- [Create React App](https://github.com/facebook/create-react-app)
- [Express](https://expressjs.com/)
- [Styled components](https://styled-components.com/)
- [React redux](https://react-redux.js.org/)
- [React router](https://reactrouter.com/)
- [Youtube Data API v3](https://developers.google.com/youtube/v3/docs)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [ESlint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Bootstrap v5](https://getbootstrap.com/)

### Host on

- [GCP Compute Engine](https://cloud.google.com/compute)

### Domain

- [Godaddy](https://sg.godaddy.com/)

### CDN

- [Cloudflaire](cloudflare.com)

### Proxy by

- [Nginx](https://www.nginx.com/)

### Running on

- [pm2](https://pm2.keymetrics.io/)

---

### `Available Scripts`

```json
// package.json --> scripts
  "scripts": {
    "dev": "./dev.sh",
    "dev:start": "nodemon ./server-build/bundle.js",
    "build:server": "rm -rvf server-build && NODE_ENV=development webpack --config webpack.config.js",
    "build:client": "rm -rvf .ssr && react-scripts build && mv build .ssr",
    "build": "yarn build:client && yarn build:server",
    "lint:fix": "eslint src/**/*.js --fix",
    "start": "yarn build && pm2 start ./server-build/bundle.js"
  },
```

### `Fast start`

```bash
yarn install && yarn start
```

### `Dependencies included`

```json
// package.json
  "dependencies": {
    "@testing-library/jest-dom": "^5.11.4",
    "@testing-library/react": "^12.0.0",
    "@testing-library/user-event": "^13.1.9",
    "axios": "^0.21.1",
    "connected-react-router": "^6.9.1",
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "format-number": "^3.0.0",
    "google-auth-library": "^7.2.0",
    "googleapis": "^80.1.0",
    "luxon": "^1.27.0",
    "nocache": "^3.0.1",
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-helmet": "^6.1.0",
    "react-redux": "^7.2.4",
    "react-router": "^5.2.0",
    "react-router-dom": "^5.2.0",
    "react-scripts": "4.0.3",
    "react-youtube": "^7.13.1",
    "redux": "^4.1.0",
    "serialize-javascript": "^6.0.0",
    "styled-components": "^5.3.0",
    "web-vitals": "^2.1.0"
  },
  "devDependencies": {
    "@babel/core": "^7.2.2",
    "@babel/preset-env": "^7.3.1",
    "babel-loader": "^8.0.5",
    "babel-plugin-styled-components": "^1.12.0",
    "css-loader": "^5.2.6",
    "eslint": "^7.30.0",
    "eslint-config-airbnb": "^18.2.1",
    "eslint-config-prettier": "^8.3.0",
    "eslint-plugin-import": "^2.23.4",
    "eslint-plugin-jsx-a11y": "^6.4.1",
    "eslint-plugin-prettier": "^3.4.0",
    "eslint-plugin-react": "^7.24.0",
    "eslint-plugin-react-hooks": "^4.2.0",
    "nodemon": "^2.0.9",
    "npm-run-all": "^4.1.5",
    "prettier": "^2.3.2",
    "style-loader": "^3.0.0",
    "svg-url-loader": "^7.1.1",
    "webpack": "^5.42.0",
    "webpack-cli": "^4.7.2",
    "webpack-dev-server": "^3.1.14",
    "webpack-node-externals": "^3.0.0"
  }
```
