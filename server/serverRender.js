import React from 'react'
import fs from 'fs'
import path from 'path'
import { StaticRouter } from 'react-router'
import { Provider as ReduxProvider } from 'react-redux'
import { renderToString } from 'react-dom/server'
import { ServerStyleSheet } from 'styled-components'
import store from '../src/reduxStore'
import App from '../src/App'
import Html from '../src/Html'

export default function renderReact({ reqUrl }) {
  const preloadedState = store.getState()
  const sheet = new ServerStyleSheet()
  const context = { preloadedState }
  const markup = renderToString(
    sheet.collectStyles(
      <StaticRouter location={reqUrl} context={context}>
        <ReduxProvider store={store}>
          <App />
        </ReduxProvider>
      </StaticRouter>
    )
  )
  const styles = sheet.getStyleTags()
  const html = Html({
    body: markup,
    styles,
    preloadedState,
  })
  const indexFile = path.resolve('./build/index.html')

  const indexFileData = fs.readFileSync(indexFile, 'utf8')
  const tmp = indexFileData.replace('<div id="root"></div>', html)
  return tmp
}
