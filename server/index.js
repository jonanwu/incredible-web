/* eslint-disable no-console */
import express from 'express'
import cors from 'cors'
import nocache from 'nocache'
import https from 'https'
import fs from 'fs'
import renderReact from './serverRender'
import { setErrorMessage } from '../src/reduxStore/actions'
import reduxStore from '../src/reduxStore'
import loadData from '../src/loadData'
import { getCurrentMatchRoute } from '../src/helper'

const PORT = process.env.PORT || 4321
const PORT_HTTPS = process.env.PORT_HTTPS || 8443
const app = express()
const privateKey = fs.readFileSync('credentials/privkey.pem', 'utf8')
const certificate = fs.readFileSync('credentials/fullchain.pem', 'utf8')
const credentials = { key: privateKey, cert: certificate }
app.use(cors())
app.use(nocache())

/**
 * @callback Handler
 * @param {express.Request}
 * @param {express.Response}
 */

/**
 * @type {Handler}
 */
async function rootHandler(req, res) {
  console.log(req.query)
  try {
    const currentRoute = getCurrentMatchRoute(req.url)
    const data = await loadData(currentRoute.loadDataType, req)
    if (req.xhr) {
      return res.json(data)
    }
    const resHtml = renderReact({ reqUrl: req.url })
    return res.send(resHtml)
  } catch (error) {
    reduxStore.dispatch(setErrorMessage(error.message))
    if (req.xhr) {
      return res.send(error.message)
    }
    const resHtml = renderReact({ reqUrl: req.url })
    return res.send(resHtml)
  }
}

app.use('/build', express.static('./build'))
app.get('*', rootHandler)

app.listen(PORT, () => {
  console.log(`Access to http://localhost:${PORT}`)
})

const httpsServer = https.createServer(credentials, app)
httpsServer.listen(PORT_HTTPS, () => {
  console.log(`Access to https://localhost:${PORT_HTTPS}`)
})
