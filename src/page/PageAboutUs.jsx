import React from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
import BaseContainer from '../common/BaseContainer'
import ProfilePicture from '../assets/profile.jpg'

const ContainerS = styled(BaseContainer)`
  flex-direction: column;
`

const PageAboutUsAcountSectionS = styled.div`
  padding: 50px;
  text-align: center;
  background-color: rgb(111 58 58);
  color: white;
  width: 100%;
`

const PageAboutUsTeamRowS = styled.div`
  &::after {
    content: '';
    clear: both;
    display: table;
  }
`
const PageAboutUsTeamColumnS = styled.div`
  float: left;
  margin-bottom: 16px;
  padding: 0 8px;
  max-width: 300px;
`

const PageAboutUsMemberCardS = styled.div`
  background: antiquewhite;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  margin: 8px;
  > img {
    width: 100%;
  }
  > .title {
    color: grey;
  }
  > .container {
    padding: 0 16px;
    &::after {
      content: '';
      clear: both;
      display: table;
    }
  }
`
const PageAboutUsH2S = styled.h2`
  text-align: center;
`

function PageAboutUsMemberCard({ name, title, desc, email, imgSrc }) {
  return (
    <PageAboutUsMemberCardS>
      <img src={imgSrc} alt={name} />
      <div className="container">
        <h2>{name}</h2>
        <p className="title">{title}</p>
        <p>{desc}</p>
        <p>{email}</p>
      </div>
    </PageAboutUsMemberCardS>
  )
}

export default function PageAboutUs() {
  return (
    <ContainerS>
      <Helmet>
        <title>About us</title>
      </Helmet>
      <PageAboutUsAcountSectionS>
        <h1>About Us Page</h1>
        <p>Some text about who we are and what we do.</p>
        <p>Resize the browser window to see that this page is responsive by the way.</p>
      </PageAboutUsAcountSectionS>
      <PageAboutUsH2S>Our Team</PageAboutUsH2S>
      <PageAboutUsTeamRowS>
        <PageAboutUsTeamColumnS>
          <PageAboutUsMemberCard
            name="Nanwu"
            title="Software engineer"
            desc="Some text that describes me lorem ipsum ipsum lorem."
            email="joram19910115@gmail.com"
            imgSrc={ProfilePicture}
          ></PageAboutUsMemberCard>
        </PageAboutUsTeamColumnS>
      </PageAboutUsTeamRowS>
    </ContainerS>
  )
}
