import React, { useMemo } from 'react'
import { Helmet } from 'react-helmet'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { DateTime } from 'luxon'
import numberFormat from 'format-number'
import BaseContainer from '../common/BaseContainer'
import FragVideo from '../components/FragVideo'
import { reduxStateType } from '../constant'

const ContainerS = styled(BaseContainer)``

const PageMovieDetailContainerRootS = styled.div`
  border-radius: 15px;
  margin: 0 auto;
`

const PageMovieDetailContainerS = styled.div`
  padding: 2px 10px;
  background-color: rgb(255 244 232);
  box-sizing: border-box;
`

const PageMovieDetailCountS = styled.p`
  font-size: 20px;
`

export default function PageMovieDetail() {
  const youtubeState = useSelector((s) => s[reduxStateType.YOUTUBE])

  /**
   * @type {import('../constant').VideoDetail}
   */
  const videoDetail = useMemo(
    () => youtubeState[reduxStateType.YOUTUBE_STATE.YTS_QUERY_INFO_RESULT] || {},
    [youtubeState]
  )
  const publishTime = useMemo(() => {
    const dt = DateTime.fromISO(videoDetail.publishTime)

    return dt.toFormat('dd LLL yyyy HH:mm:ss')
  }, [videoDetail.publishTime])

  return (
    <ContainerS>
      <Helmet>
        <title>{videoDetail.title}</title>
      </Helmet>
      <PageMovieDetailContainerRootS>
        <PageMovieDetailContainerS>
          <h3>{videoDetail.title}</h3>
        </PageMovieDetailContainerS>
        <FragVideo videoId={videoDetail.id}></FragVideo>
        <PageMovieDetailContainerS>
          <PageMovieDetailContainerS>
            <h2>
              <b>{videoDetail.title}</b>
            </h2>
            <p>{videoDetail.description}</p>
          </PageMovieDetailContainerS>
          <PageMovieDetailContainerS>
            <PageMovieDetailCountS>
              {numberFormat({ prefix: '👀 : ' })(videoDetail.viewCount)}
            </PageMovieDetailCountS>
            <PageMovieDetailCountS>
              {numberFormat({ prefix: '👍 : ' })(videoDetail.likeCount)}
            </PageMovieDetailCountS>
            <PageMovieDetailCountS>
              {numberFormat({ prefix: '👎 : ' })(videoDetail.dislikeCount)}
            </PageMovieDetailCountS>
            <p
              style={{
                textAlign: 'right',
              }}
            >{`Publish At: ${publishTime}`}</p>
          </PageMovieDetailContainerS>
        </PageMovieDetailContainerS>
      </PageMovieDetailContainerRootS>
    </ContainerS>
  )
}
