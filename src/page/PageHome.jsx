import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'
import BaseContainer from '../common/BaseContainer'
import { reduxStateType } from '../constant'
import FragVideoInfo from '../components/FragVideoInfo'

const ContainerS = styled(BaseContainer)`
  flex-direction: column;
  justify-content: flex-start;
`

export default function PageHome() {
  /**
   * @type {{ytsQueryResult:import('../constant').YTSQueryResult}}
   */
  const youtubeState = useSelector((s) => s[reduxStateType.YOUTUBE])

  const videoItemList = useMemo(
    () =>
      youtubeState[reduxStateType.YOUTUBE_STATE.YTS_QUERY_RESULT] &&
      youtubeState[reduxStateType.YOUTUBE_STATE.YTS_QUERY_RESULT].videoItemList,
    [youtubeState]
  )

  return (
    <ContainerS>
      <Helmet>
        <title>Home</title>
      </Helmet>
      {videoItemList &&
        videoItemList.map((v) => <FragVideoInfo videoItem={v} key={v.id}></FragVideoInfo>)}
    </ContainerS>
  )
}
