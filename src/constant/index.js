/* eslint-disable max-classes-per-file */
const videoType = {
  any: 'any',
  episode: 'episode',
  movie: 'movie',
}

const videoDuration = {
  any: 'any',
  long: 'long',
  medium: 'medium',
  short: 'short',
}

const order = {
  date: 'date',
  rating: 'rating',
  relevance: 'relevance',
  title: 'title',
  videoCount: 'videoCount',
  viewCount: 'viewCount',
}

const youtubeFilter = { videoType, videoDuration, order }

const reduxStateType = {
  YOUTUBE: 'youtube',
  YOUTUBE_STATE: {
    YTS_QUERY_RESULT: 'ytsQueryResult',
    YTS_QUERY_INFO_RESULT: 'ytsQueryInfoResult',
    YTS_QUERY_LAST_Q: 'ytsQueryLastQ',
  },
  ERROR_MESSAGE: 'errorMessage',
  COMMON: 'common',
  COMMON_STATE: {
    IS_FETCHING_WHILE_ROUTE_CHANGED: 'isFetchingWhileRouteChanged',
  },
}

const loadDataType = {
  ytsQuery: 'ytsQuery',
  ytsQueryInfo: 'ytsQueryInfo',
}

class PageInfo {
  constructor({ nextPageToken, prevPageToken, regionCode, totalResults }) {
    this.nextPageToken = nextPageToken
    this.prevPageToken = prevPageToken
    this.regionCode = regionCode
    this.totalResults = totalResults
  }
}

class VideoItem {
  constructor({ id, title, description, thumbnailsURL, publishTime }) {
    this.id = id
    this.title = title
    this.description = description
    this.thumbnailsURL = thumbnailsURL
    this.publishTime = publishTime
  }
}

class VideoDetail {
  constructor({
    id,
    title,
    description,
    thumbnailsURL,
    publishTime,
    viewCount,
    likeCount,
    dislikeCount,
  }) {
    this.id = id
    this.title = title
    this.description = description
    this.thumbnailsURL = thumbnailsURL
    this.publishTime = publishTime
    this.viewCount = viewCount
    this.likeCount = likeCount
    this.dislikeCount = dislikeCount
  }
}

class YTSQueryResult {
  /**
   * @param {{pageInfo:PageInfo,videoItemList:VideoItem[]}} param
   */
  constructor({ pageInfo, videoItemList }) {
    this.pageInfo = pageInfo
    this.videoItemList = videoItemList
  }
}

class YTSQueryParameters {
  constructor({ q, year, cate, order: orderTmp, dura }) {
    this.year = year
    this.cate = cate
    this.order = orderTmp
    this.dura = dura
    this.q = q
  }
}

export {
  youtubeFilter,
  reduxStateType,
  PageInfo,
  VideoItem,
  YTSQueryResult,
  VideoDetail,
  YTSQueryParameters,
  loadDataType,
}
