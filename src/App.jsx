import React from 'react'
import styled from 'styled-components'
import { Switch } from 'react-router'
import { renderRoutes } from 'react-router-config'
import BaseContainer from './common/BaseContainer'
import LinkPush from './common/LinkPush'
import Routes from './routes'
import useHistroyListener from './hooks/useHistroyListener'
import QueryPart from './common/QueryPart'
import SpinnerLoader from './common/SpinnerLoader'

const BaseContainerAppS = styled(BaseContainer)`
  display: block;
  flex-direction: column;
  box-sizing: border-box;
  max-width: 800px;
  align-self: center;
  height: auto;
  width: 100%;
  position: relative;
`

const BaseContainerMain = styled(BaseContainer)`
  /* ... */
  display: block;
`

const HeaderS = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  width: 100%;
  position: sticky;
  top: 0;
  background-color: #dbbd70b2;
  padding: 10px 0;
  z-index: 10;
`

const NavLinkS = styled(LinkPush)`
  width: 100%;
  text-align: center;
  color: palevioletred;
  border: 2px solid palevioletred;
`

function App() {
  useHistroyListener()
  return (
    <BaseContainerAppS className="BaseContainerApp">
      <HeaderS className="App-header">
        <NavLinkS to="/">Home</NavLinkS>
        <NavLinkS to="/about">About us</NavLinkS>
      </HeaderS>
      <QueryPart />
      <BaseContainerMain>
        <SpinnerLoader></SpinnerLoader>
        <Switch>{renderRoutes(Routes)}</Switch>
      </BaseContainerMain>
    </BaseContainerAppS>
  )
}

export default App
