/* eslint-disable global-require */
import { loadDataType } from './constant'
import PageAboutUs from './page/PageAboutUs'
import PageHome from './page/PageHome'
import PageMovieDetail from './page/PageMovieDetail'

const Routes = [
  {
    path: '/detail/:videoId',
    component: PageMovieDetail,
    loadDataType: loadDataType.ytsQueryInfo,
  },
  {
    path: '/about',
    component: PageAboutUs,
  },
  {
    path: '/',
    component: PageHome,
    loadDataType: loadDataType.ytsQuery,
  },
]

export default Routes
