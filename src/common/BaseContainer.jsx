import React, { useMemo } from 'react'
import { useLocation } from 'react-router-dom'
import styled from 'styled-components'

export const BaseContainerS = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
`

export default function BaseContainer(props) {
  const location = useLocation()
  const { children, ...propsRest } = props
  const [currentPath] = useMemo(
    () => location.pathname.match(/(?<=\/)\w+(?=\/)/) || ['root'],
    location.pathname
  )
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <BaseContainerS currentPath={currentPath} {...propsRest}>
      {children}
    </BaseContainerS>
  )
}
