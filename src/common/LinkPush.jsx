import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const Anchor = styled(NavLink)`
  background: transparent;
  border-radius: 3px;
  margin: 0 1em;
  padding: 0.25em 1em;
`

/**
 * @param {import('react-router-dom').NavLinkProps} props
 */
export default function LinkPush(props) {
  const { children, ...propsRest } = props
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Anchor {...propsRest}>{children}</Anchor>
}
