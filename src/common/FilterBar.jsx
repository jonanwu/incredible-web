import React from 'react'
import styled from 'styled-components'
import DropdownSelect from '../components/DropdownSelect'
import { youtubeFilter } from '../constant'

const ContainerS = styled.div`
  /*  */
`

// const FilterBarInputS = styled.input`
//   /*  */
// `
// const FilterBarButtonS = styled.button`
//   /*  */
// `

/**
 * @param {{lastQueryParams:import('../constant').YTSQueryParameters}} props
 */
export default function FilterBar({ lastQueryParams }) {
  return (
    <ContainerS>
      <DropdownSelect
        label="Year"
        options={Array(10)
          .fill(0)
          .map((_, i) => `20${21 - i}`)
          .map((v) => ({ value: v, key: v }))}
        name="year"
        defaultValue={lastQueryParams.year || '2021'}
      ></DropdownSelect>
      <DropdownSelect
        label="Category"
        options={Object.values(youtubeFilter.videoType).map((v) => ({ value: v, key: v }))}
        name="cate"
        defaultValue={lastQueryParams.cate || youtubeFilter.videoType.any}
      ></DropdownSelect>
      <DropdownSelect
        label="Order"
        options={Object.values(youtubeFilter.order).map((v) => ({ value: v, key: v }))}
        name="order"
        defaultValue={lastQueryParams.order || youtubeFilter.order.relevance}
      ></DropdownSelect>
      <DropdownSelect
        label="Duration"
        options={Object.values(youtubeFilter.videoDuration).map((v) => ({ value: v, key: v }))}
        name="dura"
        defaultValue={lastQueryParams.dura || youtubeFilter.videoDuration.any}
      ></DropdownSelect>
    </ContainerS>
  )
}
