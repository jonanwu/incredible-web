import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import qs from 'querystring'
import styled from 'styled-components'
import { reduxStateType } from '../constant'
import FilterBar from './FilterBar'
import SearchBar from './SearchBar'

const ContainerS = styled.div`
  /*  */
  /* position: sticky;
  top: ${(props) => `${props.heightTop}px` || '70px'}; */
  z-index: 1;
  background-color: aliceblue;
  width: 100%;
  padding: 12px;
`

const QueryPartFormS = styled.form`
  /*  */
  width: 100%;
  > * {
    margin-bottom: 6px;
  }
  display: flex;
  flex-direction: column;
`
const QueryPartSubmitS = styled.input`
  /*  */
`

export default function QueryPart() {
  const youtubeState = useSelector((s) => s[reduxStateType.YOUTUBE])
  const his = useHistory()
  const lastQueryParams = useMemo(
    () => youtubeState[reduxStateType.YOUTUBE_STATE.YTS_QUERY_LAST_Q] || {},
    [youtubeState]
  )

  const onFormSubmit = useCallback(
    /**
     * @param {import('react').FormEvent} event
     */
    async (event) => {
      event.preventDefault()
      const formData = new FormData(event.target)
      const formDataMap = Object.fromEntries(formData.entries())
      his.push({ pathname: '/', search: qs.stringify(formDataMap) })
    },
    []
  )

  return (
    <ContainerS>
      <QueryPartFormS onSubmit={onFormSubmit}>
        <SearchBar lastQueryParams={lastQueryParams}></SearchBar>
        <FilterBar lastQueryParams={lastQueryParams}></FilterBar>
        <QueryPartSubmitS className="btn btn-dark" type="submit" value="Submit" />
      </QueryPartFormS>
    </ContainerS>
  )
}
