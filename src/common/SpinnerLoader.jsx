import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { reduxStateType } from '../constant'

const SpinnerLoaderWrapperS = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  display: inline-flex;
  justify-content: center;
  background: #ffffffdd;
  &.none {
    display: none;
  }
`

const SpinnerLoaderS = styled.div`
  width: 10vw;
  height: 10vw;
  max-width: 200px;
  max-height: 200px;
  min-width: 80px;
  min-height: 80px;
  position: sticky;
  top: 20px;
  display: inline-flex;
  margin-top: 10vw;
  border-width: 1rem;
`

function SpinnerLoader() {
  const { [reduxStateType.COMMON_STATE.IS_FETCHING_WHILE_ROUTE_CHANGED]: isFetching } = useSelector(
    (state) => state[reduxStateType.COMMON]
  )
  const spinnerClassName = useMemo(() => (isFetching ? '' : 'none'), [isFetching])
  return (
    <SpinnerLoaderWrapperS className={spinnerClassName}>
      <SpinnerLoaderS className="spinner-border" role="status"></SpinnerLoaderS>
    </SpinnerLoaderWrapperS>
  )
}

export default SpinnerLoader
