/* eslint-disable no-underscore-dangle */
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider as ReduxProvider } from 'react-redux'
import { Helmet } from 'react-helmet'
import store from '../reduxStore'
import '../index.css'

// Create Redux store with initial state
function RootLayout(props) {
  const { children } = props
  return (
    <>
      <Helmet>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <allow-navigation href="*" />
        <allow-intent href="*" />
        <access origin="*" />
        <meta
          httpEquiv="Content-Security-Policy"
          content="default-src *;style-src * 'self' 'unsafe-inline' 'unsafe-eval'; script-src * 'self' 'unsafe-inline' 'unsafe-eval';"
        />
      </Helmet>
      <ReduxProvider store={store}>
        <BrowserRouter>{children}</BrowserRouter>
      </ReduxProvider>
    </>
  )
}

export default RootLayout
