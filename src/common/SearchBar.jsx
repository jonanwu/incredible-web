import React from 'react'
import styled from 'styled-components'

const ContainerS = styled.div`
  /*  */
`

const SearchBarInputS = styled.input`
  /*  */
`

/**
 * @param {{lastQueryParams:import('../constant').YTSQueryParameters}} props
 */
export default function SearchBar({ lastQueryParams }) {
  return (
    <ContainerS>
      <SearchBarInputS
        className="form-control"
        defaultValue={lastQueryParams.q || ''}
        type="text"
        placeholder="Search.."
        name="q"
      />
    </ContainerS>
  )
}
