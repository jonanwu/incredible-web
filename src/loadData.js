import youtubeService from './service/youtubeService'
import { loadDataType, VideoItem, YTSQueryParameters } from './constant'
import reduxStore from './reduxStore'
import { setYTSQueryInfoResult, setYTSQueryResult, setYTSQyeryLastQ } from './reduxStore/actions'
import { fetchYTSQuery, fetchYTSQueryInfo } from './fetchData'

/**
 * @param {*} type
 * @param {import('express').Request} req
 * @returns
 */
export default async (type, req) => {
  if (!global.YTS) {
    await youtubeService()
  }
  switch (type) {
    case loadDataType.ytsQuery: {
      /**
       * @type {import('../src/constant/index').YTSQueryParameters}
       */
      const ytsQueryLastQ = new YTSQueryParameters({ ...req.query })
      reduxStore.dispatch(setYTSQyeryLastQ(ytsQueryLastQ))
      const data = await fetchYTSQuery(ytsQueryLastQ)
      reduxStore.dispatch(setYTSQueryResult(data))
      return data
    }
    case loadDataType.ytsQueryInfo: {
      const videoItem = new VideoItem({ ...req.query })
      const data = await fetchYTSQueryInfo(videoItem)
      reduxStore.dispatch(setYTSQueryInfoResult(data))
      return data
    }
    default:
      return null
  }
}
