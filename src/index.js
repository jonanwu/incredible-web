import React from 'react'
import { render, hydrate } from 'react-dom'
import App from './App'
import RootLayout from './common/RootLayout'

const Application = (
  <React.StrictMode>
    <RootLayout>
      <App />
    </RootLayout>
  </React.StrictMode>
)

const root = document.getElementById('root')

if (root.hasChildNodes() === true) {
  hydrate(Application, root)
} else {
  // If we're not running on the server, just render like normal
  render(Application, root)
}
