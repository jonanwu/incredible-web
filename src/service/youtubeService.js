/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
import fs from 'fs'
import { google } from 'googleapis'
import path from 'path'
import { DateTime } from 'luxon'
import { PageInfo, VideoDetail, VideoItem, youtubeFilter, YTSQueryResult } from '../constant'
import { RFC3339 } from '../helper'

const serviceDisabled = false

const SCOPES = [
  'https://www.googleapis.com/auth/youtube.readonly',
  'https://www.googleapis.com/auth/youtubepartner',
  'https://www.googleapis.com/auth/youtube.force-ssl',
  'https://www.googleapis.com/auth/youtube',
]
const CREDENTIALS_DIR_PATH = 'credentials'
const CREDENTIAL_PATH = path.resolve(CREDENTIALS_DIR_PATH, 'google-service-account.json')
const TOKEN_PATH = path.resolve(CREDENTIALS_DIR_PATH, 'google-oauth2-token.json')

function storeToken(token) {
  try {
    fs.mkdirSync(CREDENTIALS_DIR_PATH)
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
    if (err) throw err
    console.log(`Token stored to ${TOKEN_PATH}`)
  })
}

async function authorize(jwt) {
  try {
    const tokenJSONFile = fs.readFileSync(TOKEN_PATH)
    jwt.credentials = JSON.parse(tokenJSONFile)
  } catch (error) {
    const token = await jwt.authorize()
    storeToken(token)
  }
}

const youtubeService = async () => {
  const credentialsJSONFile = fs.readFileSync(CREDENTIAL_PATH)
  const credentials = JSON.parse(credentialsJSONFile)
  const authJWT = new google.auth.JWT(
    credentials.client_email,
    null,
    credentials.private_key,
    SCOPES
  )
  authorize(authJWT)
  const service = google.youtube('v3')
  service.search.context._options.auth = authJWT

  global.YTS = service
  return service
}

/**
 * @returns {YTSQueryResult}
 */
const ytsQuery = async (
  {
    queryString = 'dog',
    order = youtubeFilter.order.relevance,
    videoDuration = youtubeFilter.videoDuration.any,
    videoType = [youtubeFilter.videoType.any],
    year = '2021',
  } = {
    queryString: 'dog',
    order: youtubeFilter.order.relevance,
    videoDuration: youtubeFilter.videoDuration.any,
    videoType: [youtubeFilter.videoType.any],
    year: '2021',
  }
) => {
  const { data } = await (async () => {
    try {
      if (serviceDisabled) {
        throw Error('serviceDisabled:true')
      }
      const publishedBefore = DateTime.utc(+year)
        .endOf('year')
        .toFormat(RFC3339)
      const publishedAfter = DateTime.utc(+year)
        .startOf('year')
        .toFormat(RFC3339)
      const res = await global.YTS.search.list({
        part: ['snippet'],
        q: queryString,
        order,
        videoType,
        videoDuration,
        maxResults: 8,
        publishedAfter,
        publishedBefore,
        type: ['video'],
      })
      return res
    } catch (error) {
      console.error(error.message)
      return { data: JSON.parse(fs.readFileSync('database/movies.json', 'utf8')) }
    }
  })()
  const { items: videoItems, ...infoRest } = data
  const pageInfo = new PageInfo({
    nextPageToken: infoRest.nextPageToken,
    prevPageToken: infoRest.prevPageToken,
    regionCode: infoRest.regionCode,
    totalResults: infoRest.pageInfo.totalResults,
  })
  const videoItemList = videoItems.map(
    (m) =>
      new VideoItem({
        id: m.id.videoId,
        title: m.snippet.title,
        description: m.snippet.description,
        thumbnailsURL: m.snippet.thumbnails.high.url,
        publishTime: m.snippet.publishTime,
      })
  )

  return new YTSQueryResult({ pageInfo, videoItemList })
}

/**
 * @param {object} props
 * @param {string} props.videoId
 * @param {VideoItem} props.videoItem
 * @returns {VideoDetail}
 */
const ytsQueryInfo = async ({ videoId, videoItem }) => {
  const { data } = await (async () => {
    try {
      if (serviceDisabled) {
        throw Error('serviceDisabled:true')
      }
      const res = await global.YTS.videos.list({
        part: ['snippet', 'statistics'],
        id: [videoId],
      })
      return res
    } catch (error) {
      console.error(error)
      return { data: JSON.parse(fs.readFileSync('database/movieDetail.json', 'utf8')) }
    }
  })()
  const {
    items: [detail],
  } = data
  const videoDetail = new VideoDetail({
    id: videoItem.id || detail.id,
    title: videoItem.title || detail.snippet.title,
    description: videoItem.description || detail.snippet.description,
    thumbnailsURL: videoItem.thumbnailsURL || detail.snippet.thumbnails.standard.url,
    publishTime: videoItem.publishTime || detail.snippet.publishedAt,
    viewCount: detail.statistics.viewCount,
    likeCount: detail.statistics.likeCount,
    dislikeCount: detail.statistics.dislikeCount,
  })

  return videoDetail
}

export { ytsQuery, ytsQueryInfo }
export default youtubeService
