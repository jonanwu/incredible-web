import serialize from 'serialize-javascript'

const Html = ({ body, styles, preloadedState }) => `
<script>
// WARNING: See the following for security issues around embedding JSON in HTML:
// https://redux.js.org/usage/server-rendering#security-considerations
window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
window.__ROUTE_DATA__ = ${serialize(preloadedState)}
</script>
${styles}
<div id="root">${body}</div>
`

export default Html
