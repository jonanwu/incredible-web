import { useLayoutEffect } from 'react'
import { useHistory } from 'react-router'
import { useDispatch } from 'react-redux'
import qs from 'querystring'
import apiService from '../service/apiService'
import { loadDataType, YTSQueryParameters } from '../constant'
import {
  setIsFetchingWhileRouteChanged,
  setYTSQueryInfoResult,
  setYTSQueryResult,
  setYTSQyeryLastQ,
} from '../reduxStore/actions'
import { getCurrentMatchRoute } from '../helper'

export default function useHistroyListener() {
  const dispatch = useDispatch()
  const history = useHistory()
  useLayoutEffect(() => {
    history.listen(async (location, action) => {
      if (action === 'PUSH') {
        dispatch(setIsFetchingWhileRouteChanged(true))
        const { data } = await apiService.get(`${location.pathname}${location.search}`)
        const currentRoute = getCurrentMatchRoute(location.pathname)
        const { loadDataType: type } = currentRoute
        switch (type) {
          case loadDataType.ytsQuery: {
            const query = qs.parse(location.search.replace('?', ''))
            const ytsQueryLastQ = new YTSQueryParameters(query)
            dispatch(setYTSQyeryLastQ(ytsQueryLastQ))
            dispatch(setYTSQueryResult(data))
            break
          }
          case loadDataType.ytsQueryInfo: {
            dispatch(setYTSQueryInfoResult(data))
            break
          }
          default: {
            break
          }
        }
        dispatch(setIsFetchingWhileRouteChanged(false))
      }
    })
  }, [])
}
