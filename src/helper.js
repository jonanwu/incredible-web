import { matchPath } from 'react-router'
import Routes from './routes'

/* eslint-disable import/prefer-default-export */
export const isServer = !(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
)

export const RFC3339 = "yyyy-LL-dd'T'TT'Z'"

export const getCurrentMatchRoute = (pathname) =>
  Routes.find((route) => matchPath(pathname, route)) || {}
