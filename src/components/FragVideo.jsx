import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { reduxStateType } from '../constant'
import './FragVideo.css'

const YouTubeS = styled.iframe`
  width: 100%;
  height: 300px;
`

export default function FragVideo({ videoId }) {
  const iframeSrc = useMemo(() => `https://www.youtube.com/embed/${videoId}?autoplay=1`, [videoId])
  const isDisplay = useSelector(
    (state) =>
      !state[reduxStateType.COMMON][reduxStateType.COMMON_STATE.IS_FETCHING_WHILE_ROUTE_CHANGED]
  )

  return isDisplay && <YouTubeS allow="autoplay" frameborder="0" src={iframeSrc} />
}
