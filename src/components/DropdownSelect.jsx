import React from 'react'
import styled from 'styled-components'

const ContainerS = styled.div`
  /*  */
`

const DropdownSelectLabelS = styled.label`
  /*  */
`
const DropdownSelectSelectS = styled.select`
  /*  */
`
const DropdownSelectOptionS = styled.option`
  /*  */
`

export default function DropdownSelect({
  label = 'Choose a car:',
  options = [],
  name = 'cars',
  defaultValue = 'any',
}) {
  return (
    <ContainerS>
      <DropdownSelectLabelS htmlFor={name}>{label}</DropdownSelectLabelS>
      <DropdownSelectSelectS className="form-select" name={name}>
        {options.map((op) => (
          <DropdownSelectOptionS
            selected={op.value === defaultValue}
            key={op.value}
            value={op.value}
          >
            {op.key}
          </DropdownSelectOptionS>
        ))}
      </DropdownSelectSelectS>
    </ContainerS>
  )
}
