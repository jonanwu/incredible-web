import React, { useMemo } from 'react'
import qs from 'querystring'
// import { useHistory } from 'react-router'
import styled from 'styled-components'
import { DateTime } from 'luxon'
import { Link } from 'react-router-dom'

const FragVideoInfoS = styled(Link)`
  background: white;
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  margin: 10px 0;
  text-decoration: none;
  cursor: pointer;
  color: black;
  &:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.8);
    transform: translate(-4px, -8px);
  }
`

const FragVideoInfoImgS = styled.img`
  width: 100%;
`
const FragVideoInfoContentS = styled.div`
  padding: 2px 16px;
`
const FragVideoInfoTitleS = styled.h4`
  /* ... */
`
const FragVideoInfoDesS = styled.p`
  overflow-wrap: break-word;
`
const FragVideoInfoFootS = styled.footer`
  padding: 10px;
  text-align: right;
  color: brown;
`

/**
 * @param {Object} props
 * @param {import('../constant').VideoItem} props.videoItem
 */
export default function FragVideoInfo({ videoItem }) {
  const queryString = useMemo(() => qs.stringify(videoItem), [videoItem])

  const toVideoPath = useMemo(() => {
    const pathname = `/detail/${videoItem.id}`
    const search = queryString
    return `${pathname}?${search}`
  }, [queryString])

  const publishTime = useMemo(() => {
    const dt = DateTime.fromISO(videoItem.publishTime)

    return dt.toFormat('dd.LLL.yyyy HH:mm:ss')
  }, [videoItem.publishTime])

  return (
    <FragVideoInfoS to={toVideoPath}>
      <FragVideoInfoImgS src={videoItem.thumbnailsURL} alt={videoItem.title} />
      <FragVideoInfoContentS>
        <FragVideoInfoTitleS>{videoItem.title}</FragVideoInfoTitleS>
        <FragVideoInfoDesS>{videoItem.description}</FragVideoInfoDesS>
      </FragVideoInfoContentS>
      <FragVideoInfoFootS>{publishTime}</FragVideoInfoFootS>
    </FragVideoInfoS>
  )
}
