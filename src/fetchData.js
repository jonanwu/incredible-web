import { ytsQuery, ytsQueryInfo } from './service/youtubeService'

const fetchYTSQuery = (ytsQueryLastQ) =>
  ytsQuery({
    queryString: ytsQueryLastQ.q,
    order: ytsQueryLastQ.order,
    videoDuration: ytsQueryLastQ.dura,
    videoType: ytsQueryLastQ.cate,
    year: ytsQueryLastQ.year,
  })

const fetchYTSQueryInfo = (videoItem) => ytsQueryInfo({ videoId: videoItem.id, videoItem })

export { fetchYTSQuery, fetchYTSQueryInfo }
