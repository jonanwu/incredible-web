import actionTypes from '../actionTypes'

function setYTSQueryResult(data) {
  return {
    type: actionTypes.SetYTSQueryResult,
    data,
  }
}
function setYTSQueryInfoResult(data) {
  return {
    type: actionTypes.SetYTSQueryInfoResult,
    data,
  }
}

function setErrorMessage(message) {
  return {
    type: actionTypes.SetErrorMessage,
    data: message,
  }
}

/**
 * @param {import('../../constant').YTSQueryParameters} data
 */
function setYTSQyeryLastQ(data) {
  return {
    type: actionTypes.SetYTSQyeryLastQ,
    data,
  }
}

/**
 * @param {boolean} isFetching
 */
function setIsFetchingWhileRouteChanged(isFetching) {
  return {
    type: actionTypes.SetIsFetchingWhileRouteChanged,
    data: isFetching,
  }
}

export {
  setYTSQueryResult,
  setYTSQueryInfoResult,
  setErrorMessage,
  setYTSQyeryLastQ,
  setIsFetchingWhileRouteChanged,
}
