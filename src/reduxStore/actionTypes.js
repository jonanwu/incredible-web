export default {
  SetYTSQueryResult: 'set video items from by youtube service',
  SetYTSQueryInfoResult: 'set video detail',
  SetErrorMessage: 'set error message',

  SetYTSQyeryLastQ: 'set last query string of search',

  SetIsFetchingWhileRouteChanged: 'set is it fetching while current route changed',
}
