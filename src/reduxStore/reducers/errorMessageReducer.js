import actionTypes from '../actionTypes'

// State argument is not application state, only the state this reducer is responsible for
export default function errorMessageReducer(state = null, action) {
  switch (action.type) {
    case actionTypes.SetErrorMessage: {
      const { data } = action
      return { ...state, data }
    }
    default:
      return state
  }
}
