// import actionTypes from '../actionTypes'

import actionTypes from '../actionTypes'

// State argument is not application state, only the state this reducer is responsible for
export default function youtubeReducer(state = null, action) {
  switch (action.type) {
    case actionTypes.SetYTSQueryResult: {
      const { data: ytsQueryResult } = action
      return { ...state, ytsQueryResult }
    }
    case actionTypes.SetYTSQueryInfoResult: {
      const { data: ytsQueryInfoResult } = action
      return { ...state, ytsQueryInfoResult }
    }
    case actionTypes.SetYTSQyeryLastQ: {
      const { data: ytsQueryLastQ } = action
      return { ...state, ytsQueryLastQ }
    }
    default:
      return state
  }
}
