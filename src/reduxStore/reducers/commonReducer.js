import actionTypes from '../actionTypes'

// State argument is not application state, only the state this reducer is responsible for
export default function commonReducer(state = { isFetchingWhileRouteChanged: false }, action) {
  switch (action.type) {
    case actionTypes.SetIsFetchingWhileRouteChanged: {
      const { data } = action
      return { ...state, isFetchingWhileRouteChanged: data }
    }
    default:
      return state
  }
}
