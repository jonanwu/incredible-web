import { combineReducers } from 'redux'
import youtube from './youtubeReducer'
import errorMessage from './errorMessageReducer'
import common from './commonReducer'

const rootReducer = combineReducers({
  youtube,
  errorMessage,
  common,
})

export default rootReducer
