/* eslint-disable no-underscore-dangle */
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { reduxStateType } from '../constant'
import { isServer } from '../helper'
import reducers from './reducers'

const store = () => {
  if (isServer) {
    return createStore(
      reducers,
      {
        [reduxStateType.YOUTUBE]: {},
        [reduxStateType.ERROR_MESSAGE]: null,
        [reduxStateType.COMMON]: {
          [reduxStateType.COMMON_STATE.IS_FETCHING_WHILE_ROUTE_CHANGED]: false,
        },
      },
      applyMiddleware(thunk)
    )
  }
  // Grab the state from a global variable injected into the server-generated HTML
  const preloadedState = window.__PRELOADED_STATE__
  // Allow the passed state to be garbage-collected
  delete window.__PRELOADED_STATE__
  return createStore(reducers, preloadedState, composeWithDevTools(applyMiddleware(thunk)))
}

export default store()
